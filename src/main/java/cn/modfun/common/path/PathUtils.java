package cn.modfun.common.path;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;

public class PathUtils {

	/**
	 * 获得web工程目录，以/结尾
	 * @return	String	工程文件路径，以/结尾
	 */
	public static String getWebRoot() {
		URL url = PathUtils.class.getResource(PathUtils.class.getSimpleName()
				+ ".class");
		String str = url.getPath();
		/* 这个判断是用来若是打成jar包的情况下需要截取 */
		if(str.indexOf("file:") == 0){
			str = str.substring(5);
		}
		if (str.indexOf(":") != -1) {
			str = str.substring(1, str.lastIndexOf("WEB-INF"));// 在windows下面为/F:/MyPrj/WORK/post/
		} else {
			str = str.substring(0, str.lastIndexOf("WEB-INF"));// 在Linux下面为/usr/local/apache.../
		}
		// str = URLDecoder.decode(str, "UTF-8");
		return str;
	}
	
	/**
	 * 获得web classpath
	 * @return	String classes路径，以/结尾
	 */
	public static String getWebRootClasspath(){
		return getWebRoot()+"WEB-INF/classes/";
	}
	
	public static String getPath(){
		String os_name = System.getProperties().get("os.name").toString().toLowerCase();
		if(os_name.indexOf("windows")!=-1){
			return PathUtils.class.getClassLoader().getResource("\\").getPath();
		}else if(os_name.indexOf("linux")!=-1){
			return PathUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		}
		return null;
	}
	
	public static String getJarPath() {  
        URL url = PathUtils.class.getProtectionDomain().getCodeSource().getLocation();  
        String filePath = null;  
        try {  
            filePath = URLDecoder.decode(url.getPath(), "utf-8");// 转化为utf-8编码  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        if (null != filePath && filePath.endsWith(".jar")) {// 可执行jar包运行的结果里包含".jar"
            // 截取路径中的jar包名  
            filePath = filePath.substring(0, filePath.lastIndexOf("/") + 1);  
        }  
        File file = new File(filePath);  
        filePath = file.getAbsolutePath();//得到windows下的正确路径  
        return filePath;  
    }
}
