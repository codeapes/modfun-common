package cn.modfun.common.security;


import java.io.UnsupportedEncodingException;
import java.util.UUID;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Base64Utils {

	/**
	 * 加密
	 * @param str
	 * @return
	 */
    public static String encrypt(String str) {  
        byte[] b = null;  
        String s = null;  
        try {  
            b = str.getBytes("utf-8");  
        } catch (UnsupportedEncodingException e) {  
            e.printStackTrace();  
        }  
        if (b != null) {  
            s = new BASE64Encoder().encode(b);  
        }  
        return s;  
    }  
  
    /**
     * 解密
     * @param s
     * @return
     */
    public static String decrypt(String s) {  
        byte[] b = null;  
        String result = null;  
        if (s != null) {  
            BASE64Decoder decoder = new BASE64Decoder();  
            try {  
                b = decoder.decodeBuffer(s);  
                result = new String(b, "utf-8");  
            } catch (Exception e) {  
                e.printStackTrace();  
            }  
        }  
        return result;  
    }
    
    public static void main(String[] args) {
    	System.out.println(UUID.randomUUID().toString().toUpperCase());
//    	String content = Base64Util.decrypt("PDE1OD5DRUY6MHxUaHJlYXQgRWFybHkgV2FybmluZyBTeXN0ZW18VGhyZWF0IEVhcmx5IFdhcm5pbmcgU3lzdGVtfDMuNzEuMTE1MXw1NjB8QSBwcml2aWxlZ2VkIHVzZXIgYXR0ZW1wdGVkIHRvIGxvZyBvbiB0byBNeVNRTCBzZXJ2aWNlfDJ8ZHZjPTE5Mi4xNjguMTE4LjkzIGRldmljZU1hY0FkZHJlc3M9MDA6RTA6NjY6REM6NDQ6NzQgZHZjaG9zdD1sb2NhbGhvc3QgZGV2aWNlRXh0ZXJuYWxJZD05OTg3RDMyOUE0NjctNDc0OTk0M0QtQzI5Ri0zRkI2LTZCRTMgcnQ9SnVuIDE2IDIwMTUgMDk6NDU6MDAgR01UKzA4OjAwIGFwcD1NWVNRTCBkZXZpY2VEaXJlY3Rpb249MSBkaG9zdD0xOTIuMTY4LjExOC42NCBkc3Q9MTkyLjE2OC4xMTguNjQgZHB0PTMzMDYgZG1hYz0yODo1MTozMjowNzpGMToxMCBzaG9zdD1saW5jaC1hc3VzIHNyYz0xOTIuMTY4LjExOS4xMDggc3B0PTUxMDEwIHNtYWM9QUM6MjI6MEI6NTc6QTI6MDIgZmlsZVR5cGU9MCBmc2l6ZT0wIGFjdD1ub3QgYmxvY2tlZCBjbjNMYWJlbD1UaHJlYXQgVHlwZSBjbjM9MiBkZXN0aW5hdGlvblRyYW5zbGF0ZWRBZGRyZXNzPTE5Mi4xNjguMTE4LjY0IGZpbGVIYXNoPTAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAgc291cmNlVHJhbnNsYXRlZEFkZHJlc3M9MTkyLjE2OC4xMTkuMTA4IHN1aWQ9cm9vdCBjbnQ9NiBjYXQ9QXV0aGVudGljYXRpb24=");
//    	String[] lines = content.split("[|]");
//    	for(String line : lines){
//    		System.out.println(line);
//    		if(line.indexOf("=")!=-1){
//    			String[] kvs = line.split("=");
//    			for(String kv : kvs){
//    				System.out.println(kv);
//    			}
//    		} 
//    	}
    	System.out.println(UUID.randomUUID().toString());
    	System.out.println(Base64Utils.decrypt("eyJhdHRhY2tfdGltZSI6IjIwMTUtMDktMjMgMDk6MzA6NDIiLCJjbGllbnRfaWQiOiJOQVNXUy1DNTUxQzRCQS0yNzcwLTQ3MDctQUYxRS1DRkVDOTUyRjcwNEUtMTUwODE1IiwiY2xpZW50X2lwIjoiMTI3LjAuMC4xIiwiY29tYmluaWQiOi04NTg5OTM0NjAsImRlZmVuZF9vcGVyX3R5cGUiOjEsImRlc3RfaXAiOiIwLjAuMC4wIiwiZGVzdF9wb3J0Ijo4MCwiaG9zdCI6IjE5Mi4xNjguMTE5LjE0MiIsImhvc3RfaWQiOjAsImh0dHBfYXR0YWNrX2NvbnRlbnQiOiJHRVQgLy9tb2R1bGVzLnBocD9uYW1lPVNlY3Rpb25zJm9wPWxpc3RhcnRpY2xlcyZzZWNpZD0tMSUyMFVOSU9OJTIwU0VMRUNUJTIwcHdkJTIwRlJPTSUyMG51a2VfYXV0aG9ycyAgSFRUUC8xLjFcclxuQWNjZXB0LUVuY29kaW5nOmlkZW50aXR5XHJcbkhvc3Q6MTkyLjE2OC4xMTkuMTQyXHJcblxyXG4iLCJodHRwX21ldGhvZCI6IkdFVCIsImh0dHBfdXJsIjoiLy9tb2R1bGVzLnBocD9uYW1lPVNlY3Rpb25zJm9wPWxpc3RhcnRpY2xlcyZzZWNpZD0tMSUyMFVOSU9OJTIwU0VMRUNUJTIwcHdkJTIwRlJPTSUyMG51a2VfYXV0aG9ycyIsImljb3VudCI6LTg1ODk5MzQ2MCwibWF0Y2hfbmFtZSI6IlJFUVVFU1RfUFJPVE9DT0wiLCJtYXRjaF9yZWciOiIgSFRUUC8xLjEiLCJtYXRjaF92YWx1ZSI6IiBIVFRQLzEuMSIsIm1zZ19pZCI6ImI0MzE4MDI4LTYxOTItMTFlNS05NDY2LTAwMGMyOWI1NzY1OSIsIm9yZ19pZCI6MCwicGF0dGVybiI6MTEwMDA1MCwic2l0ZV9pZCI6MCwic3JjX2lwIjoiMTkyLjE2OC4xMTkuMTA4Iiwic3JjX3BvcnQiOjY1MjIzLCJ1c2VyX2FnZW50IjoiIn0K"));
	}
}
