package cn.modfun.common.security;


import javax.crypto.Cipher;
import java.security.Key;
import java.security.Security;

public class CryptUtils {

	/** 加密工具 */
	private Cipher encryptCipher = null;

	/** 解密工具 */
	private Cipher decryptCipher = null;
	/* 0x4D, 0x21, 0x4F, 0x40, 0x36, 0x26, 0x26, 0x4C,
			0x4D, 0x21, 0x4F, 0x40, 0x36, 0x26, 0x24, 0x4C */
	private byte[] keys = { 0x4D, 0x21, 0x4F, 0x40, 0x36, 0x26, 0x24, 0x4C, 0x4D, 0x21, 0x4F, 0x40, 0x36, 0x26,0x24,0x4C };
	
	private String encoding = "GBK";

	/**
	 * 加密/解密算法/工作模式/填充方式
	 * */
	private static final String CIPHER_ALGORITHM = "DES/ECB/NoPadding";

	/**
	 * 将byte数组转换为表示16进制值的字符串， 如：byte[]{8,18}转换为：0813， 和public static byte[]
	 * hexStr2ByteArr(String strIn) 互为可逆的转换过程
	 * 
	 * @param arrB
	 *            需要转换的byte数组
	 * @return 转换后的字符串
	 * @throws Exception
	 *             本方法不处理任何异常，所有异常全部抛出
	 */
	public static String byteArrayToHexString(byte[] arrB) throws Exception {
		int iLen = arrB.length;
		// 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍
		StringBuffer sb = new StringBuffer(iLen * 2);
		for (int i = 0; i < iLen; i++) {
			int intTmp = arrB[i];
			// 把负数转换为正数
			while (intTmp < 0) {
				intTmp = intTmp + 256;
			}
			// 小于0F的数需要在前面补0
			if (intTmp < 16) {
				sb.append("0");
			}
			sb.append(Integer.toString(intTmp, 16));
		}
		return sb.toString();
	}

	/**
	 * 将表示16进制值的字符串转换为byte数组， 和public static String byteArr2HexStr(byte[] arrB)
	 * 互为可逆的转换过程
	 * 
	 * @param strIn
	 *            需要转换的字符串
	 * @return 转换后的byte数组
	 * @throws Exception
	 *             本方法不处理任何异常，所有异常全部抛出
	 * @author <a href="mailto:leo841001@163.com">LiGuoQing</a>
	 */
	public static byte[] hexStringToByteArray(String strIn) throws Exception {
		byte[] arrB = strIn.getBytes();
		int iLen = arrB.length;

		// 两个字符表示一个字节，所以字节数组长度是字符串长度除以2
		byte[] arrOut = new byte[iLen / 2];
		for (int i = 0; i < iLen; i = i + 2) {
			String strTmp = new String(arrB, i, 2);
			arrOut[i / 2] = (byte) Integer.parseInt(strTmp, 16);
		}
		return arrOut;
	}

	/**
	 * 指定密钥构造方法
	 * 
	 *            指定的密钥
	 * @throws Exception
	 */
	private void init(byte[] keys) throws Exception {

		Security.addProvider(new com.sun.crypto.provider.SunJCE());
		Key key = getKey(keys);

		encryptCipher = Cipher.getInstance(CIPHER_ALGORITHM);
		encryptCipher.init(Cipher.ENCRYPT_MODE, key);

		decryptCipher = Cipher.getInstance(CIPHER_ALGORITHM);
		decryptCipher.init(Cipher.DECRYPT_MODE, key);
	}

	/**
	 * 默认构造方法，使用默认密钥
	 * 
	 * @throws Exception
	 */
	public CryptUtils() throws Exception {
		init(getKeyArray());
	}
	
	public CryptUtils(String encoding) throws Exception {
		this.encoding = encoding;
		init(getKeyArray());
	}

	public CryptUtils(byte[] keys) throws Exception {
		this.keys = keys;
		init(keys);
	}
	
	public CryptUtils(byte[] keys,String encoding) throws Exception {
		this.keys = keys;
		this.encoding = encoding;
		init(keys);
	}

	public byte[] getKeyArray() {
		int iCheckSum = 0;
		for (int i = 0; i < this.keys.length; i++) {
			iCheckSum += (int) this.keys[i];
		}
		iCheckSum = iCheckSum % 128;

		keys[7] = (byte) iCheckSum;
		keys[15] = keys[7];

		keys[3] = (byte) ((iCheckSum | (int) keys[3]) % 128);
		keys[11] = keys[3];

		return keys;
	}

	/**
	 * 加密字节数组
	 * 
	 * @param arrB
	 *            需加密的字节数组
	 * @return 加密后的字节数组
	 * @throws Exception
	 */
	public byte[] encrypt(byte[] arrB) throws Exception {
		return encryptCipher.doFinal(arrB);
	}

	/**
	 * 加密字符串
	 * 
	 * @param strIn
	 *            需加密的字符串
	 * @return 加密后的字符串
	 * @throws Exception
	 */
	public String encrypt(String strIn) throws Exception {

		int length = strIn.getBytes(this.encoding).length;

		int paddingCount = 0;
		if (length % 8 == 0) {
			return byteArrayToHexString(encrypt(strIn.getBytes(this.encoding)));
		} else {
			paddingCount = 8 - length % 8;
			byte[] data = new byte[length + paddingCount];
			System.arraycopy(strIn.getBytes(this.encoding), 0, data, 0, length);

			for (int i = 0; i < paddingCount; i++) {
				data[length + i] = 0;
			}
			return byteArrayToHexString(encrypt(data));
		}
	}

	/**
	 * 解密字节数组
	 * 
	 * @param arrB
	 *            需解密的字节数组
	 * @return 解密后的字节数组
	 * @throws Exception
	 */
	public byte[] decrypt(byte[] arrB) throws Exception {
		return decryptCipher.doFinal(arrB);
	}

	/**
	 * 解密字符串
	 * 
	 * @param strIn
	 *            需解密的字符串
	 * @return 解密后的字符串
	 * @throws Exception
	 */
	public String decrypt(String strIn) throws Exception {
		return new String(decrypt(hexStringToByteArray(strIn)), this.encoding).trim();
	}

	/**
	 * 从指定字符串生成密钥，密钥所需的字节数组长度为8位 不足8位时后面补0，超出8位只取前8位
	 * 
	 * @param arrBTmp
	 *            构成该字符串的字节数组
	 * @return 生成的密钥
	 * @throws java.lang.Exception
	 */
	private Key getKey(byte[] arrBTmp) throws Exception {
		// 创建一个空的8位字节数组（默认值为0）
		byte[] arrB = new byte[8];

		// 将原始字节数组转换为8位
		for (int i = 0; i < arrBTmp.length && i < arrB.length; i++) {
			arrB[i] = arrBTmp[i];
		}

		// 生成密钥
		Key key = new javax.crypto.spec.SecretKeySpec(arrB, "DES");

		return key;
	}

	public static void main(String[] args) {
		try {
			// String test = "Liurenwangan六壬网安加解密中文测试123456ABCDEFG123456789测试";
			// String dest
			// ="e0b085edea2e97a593ef9f4e273c669b5784a809956b79a35be8e4da15a863d7001436aa480117500d1b2ddd58e88e5d8302f626d4a526604de061dab1d87f108f4d6e79e4a123ce80e9f3f10d07fd7bd075cba82ead68a3";
			//
			CryptUtils des = new CryptUtils();// 自定义密钥
			System.out.println(des.encrypt("{msg_type:'8000',msg_id:'asfd-asfd-asfd',data:{key:'56f0e99654d6b1fe53b1a6fb',mac:'3E-3D-4S-5A-DF-5X',lic:'',rand_key:'sd3dlsfdsafdsd42'}}"));
			System.out.println(des.decrypt(des.encrypt("ndasec-db-123")));
			// System.out.println("加密前的字符Java：" + test);
			// String dest1 = des.encrypt(test);
			//
			// System.out.println("加密后的字符Java：" + dest1);
			// System.out.println("解密后的字符Java：" + des.decrypt(dest1));
			//
			//
			// System.out.println("加密后的字符   C++：" + dest);
			// System.out.println("解密后的字符   C++：" + des.decrypt(dest));
			//
			// System.out.println(des.encrypt("ndasec-hps-123"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
