package cn.modfun.common.notity;

import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import cn.hutool.setting.Setting;
import cn.modfun.common.lang.ChainMap;
import cn.modfun.common.lang.Result;
import cn.modfun.common.lang.Error;
import cn.modfun.common.lang.StringUtils;

public class AliyunSMSUtils {

	//产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";
    
    static String defaultConnectTimeout = "10000";
    
    static String defaultReadTimeout = "10000";
    
    static Setting aliyunsms = null;
    
    static{
    	aliyunsms = new Setting("config/aliyunsms.txt");
    	for(String g : aliyunsms.getGroups()){
    		System.out.println(g);
    	}
    }
	/**
	 * 发送短信
	 * @param group			配置key
	 * @param phoneNumbers	手机号码
	 * @param templateCode	模板编码
	 * @param templateParam	模板参数
	 * @param outId			
	 * @return
	 */
	public static Result send(String group,String phoneNumbers,String templateCode,Map<String,Object> templateParam,String outId){
		Setting setting = aliyunsms.getSetting(group);
		System.out.println(aliyunsms);
		System.out.println(setting);
		/* 复写参数 */
		if(!StringUtils.isBlank(setting.getStr("sun.net.client.defaultConnectTimeout"))){
			defaultConnectTimeout = setting.getStr("sun.net.client.defaultConnectTimeout");
		}
		if(!StringUtils.isBlank(setting.getStr("sun.net.client.defaultReadTimeout"))){
			defaultReadTimeout = setting.getStr("sun.net.client.defaultReadTimeout");
		}
		/* 初始化acsClient,暂不支持region化 */
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", setting.getStr("accessKeyId"), setting.getStr("accessKeySecret"));
        try{
        	DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
          //组装请求对象-具体描述见控制台-文档部分内容
            SendSmsRequest request = new SendSmsRequest();
            //必填:待发送手机号
            request.setPhoneNumbers(phoneNumbers);
            //必填:短信签名-可在短信控制台中找到
            request.setSignName(setting.getStr("signName"));
            //必填:短信模板-可在短信控制台中找到
            request.setTemplateCode(templateCode);
            //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            request.setTemplateParam(JSON.toJSONString(templateParam));

            //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
            //request.setSmsUpExtendCode("90997");

            //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
            request.setOutId(outId);

            //hint 此处可能会抛出异常，注意catch
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            return new Result("OK".equals(sendSmsResponse.getCode()),sendSmsResponse.getMessage()).setData(sendSmsResponse);
        }catch(Exception e){
        	e.printStackTrace();
        	return new Result(new Error("fail",e.getLocalizedMessage()));
        }
	}
	/**
	 * 
	 * @param group
	 * @param phoneNumbers
	 * @param templateCode
	 * @param templateParam
	 * @return
	 */
	public static Result send(String group,String phoneNumbers,String templateCode,Map<String,Object> templateParam){
		return send(group,phoneNumbers,templateCode,templateParam,null);
	}
	
	public static void main(String[] args) {
		System.out.println(send("yisaoton","18650763733,18650761307","SMS_109475342",new ChainMap().put("code", 8888)));
	}
	
	public static Result sendByTemplate(String group,String phoneNumbers,String templateCode,Map<String,Object> templateParam){
		templateCode = aliyunsms.getByGroup(templateCode, "_template_");
		if(StringUtils.isBlank(templateCode)){
			return new Result(new Error("missing template config","配置文件中缺少短信模板配置信息"));
		}
		return send(group,phoneNumbers,templateCode,templateParam,null);
	}
}
