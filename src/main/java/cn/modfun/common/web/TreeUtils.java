package cn.modfun.common.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TreeUtils {
	
	/**
	 * 列表转树
	 * @param idKey
	 * @param pidKey
	 * @param childrenKey
	 * @param openKey
	 * @param folderKey
	 * @param rootNode
	 * @param allList
	 * List<Map<String,Object>> tree = new ArrayList<Map<String,Object>>();
		
		Map<String,Object> rootNode = new HashMap<String,Object>();
		rootNode.put("resource_id", 0l);
		rootNode.put("resource_pid",-1l);
		rootNode.put("resource_name", "ROOT");
		rootNode.put("expand",true);
		rootNode.put("leaf",true);
		tree.add(rootNode);

		tree.addAll(resources);
		
		TreeUtil.listToTree("resource_id", "resource_pid", "children", rootNode, tree);
		tree.clear();
		tree.add(rootNode);
	 */
	public static void listToTree(String idKey,String pidKey,String childrenKey,String openKey,String folderKey, Map<String,Object> rootNode,List<Map<String,Object>> allList){
		allList.remove(rootNode);// 删除掉本身。
		List<Map<String,Object>> tempList = new ArrayList<Map<String,Object>>();// 除去已被用过栏目的集合
		tempList.addAll(allList);
		List<Map<String,Object>> children = new ArrayList<Map<String,Object>>();// 本级子栏目
		
		for (Map<String,Object> node : allList) {
			if (rootNode.get(idKey)!=null && node.get(pidKey) !=null && rootNode.get(idKey).toString().equals(node.get(pidKey).toString())) {
				children.add(node);
				tempList.remove(node);
			}
		}
		rootNode.put(folderKey,false);
		if (children.size() == 0) {
			return;
		} else {
			rootNode.put(childrenKey, children);
			rootNode.put(folderKey,true);
			rootNode.put(openKey,true);
			for (Map<String,Object> temp : children) {
				listToTree(idKey,pidKey,childrenKey,openKey,folderKey,temp, tempList);
			}
		}
	}
	public static void listToTree(String idKey,String pidKey,String childrenKey,Map<String,Object> rootNode,List<Map<String,Object>> allList){
		allList.remove(rootNode);// 删除掉本身。
		List<Map<String,Object>> tempList = new ArrayList<Map<String,Object>>();// 除去已被用过栏目的集合
		tempList.addAll(allList);
		List<Map<String,Object>> children = new ArrayList<Map<String,Object>>();// 本级子栏目
		for (Map<String,Object> node : allList) {
			if (rootNode.get(idKey)!=null && node.get(pidKey) !=null && rootNode.get(idKey).toString().equals(node.get(pidKey).toString())) {
				children.add(node);
				tempList.remove(node);
			}
		}
		rootNode.put("leaf",true);
		if (children.size() == 0) {
			return;
		} else {
			rootNode.put(childrenKey, children);
			rootNode.put("leaf",false);
			rootNode.put("expanded",true);
			for (Map<String,Object> temp : children) {
				listToTree(idKey,pidKey,childrenKey,temp, tempList);
			}
		}
	}
	public static void listToTree(String idKey,String pidKey,String childrenKey,Map<String,Object> rootNode,List<Map<String,Object>> allList,int depth){
		if(depth==1){
			return ;
		}
		allList.remove(rootNode);// 删除掉本身。
		List<Map<String,Object>> tempList = new ArrayList<Map<String,Object>>();// 除去已被用过栏目的集合
		tempList.addAll(allList);
		List<Map<String,Object>> children = new ArrayList<Map<String,Object>>();// 本级子栏目
		for (Map<String,Object> node : allList) {
			if (rootNode.get(idKey)!=null && node.get(pidKey) !=null && rootNode.get(idKey).toString().equals(node.get(pidKey).toString())) {
				children.add(node);
				tempList.remove(node);
			}
		}
		rootNode.put("leaf",true);
		if (children.size() == 0) {
			return;
		} else {
			rootNode.put(childrenKey, children);
			rootNode.put("leaf",false);
			rootNode.put("expanded",true);
			for (Map<String,Object> temp : children) {
				listToTree(idKey,pidKey,childrenKey,temp, tempList,depth-1);
			}
		}
	}
}
