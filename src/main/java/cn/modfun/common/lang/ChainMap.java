package cn.modfun.common.lang;


import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class ChainMap extends LinkedHashMap<String, Object> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5581165973409737213L;

	public ChainMap put(String key,Object value){
		super.put(key, value);
		return this;
	}
	
	public ChainMap(){}
	
	public ChainMap(Map<String,Object> map){
		super.putAll(map);
	}
}
