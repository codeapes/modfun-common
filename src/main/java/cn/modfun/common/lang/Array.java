package cn.modfun.common.lang;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * 模拟JS的array功能
 * @Description 
 * @version 1.0 
 * @author LinCH - CodeApes.Club (312329768@qq.com)
 * @date 2015年5月9日 上午9:07:19
 */
public class Array {
	
	/**
	 * 定义变量
	 */
	/*
	 * 定义存储的变量
	 */
	private List<String> array = new ArrayList<String>();
	/*
	 * 定义默认的分隔符
	 */
	private String separator = ",";
	/*
	 * 设置默认日期格式化
	 */
	private String datePattern = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 设置需要转换的日期格式
	 * @param datePattern
	 */
	public void setDatePattern(String datePattern) {
		this.datePattern = datePattern;
	}

	/**
	 * 构造函数
	 */
	/**
	 * 默认构造函数
	 */
	public Array(){}
	/**
	 * 默认初始化单个参数的构造函数
	 * @param obj
	 */
	public Array(Object obj){
		push(obj);
	}
	/**
	 * 默认初始化单个或者多个参数的构造函数
	 * @param objs
	 */
	public Array(Object[] objs){
		if(objs != null){
			for(Object obj : objs){
				array.add(instanceOf(obj));
			}
		}
	}
	/**
	 * 
	 * @param objs
	 */
	public Array(List<Object> objs){
		if(objs != null){
			for(Object obj : objs){
				array.add(instanceOf(obj));
			}
		}
	}
	/**
	 * 转换为字符串
	 * @param separator 分隔符
	 * @return 字符串
	 */
	public String toString(String separator){
		if(separator!=null){
			this.separator = separator;
		}
		return join(separator);
	}
	/**
	 * 转换为字符串
	 * <p>默认无分隔符</p>
	 * @return 字符串
	 */
	public String toString(){
		this.separator = "";
		return join(separator);
	}
	
	/**
	 * 添加
	 * @param obj
	 */
	public Array push(Object obj){
		array.add(instanceOf(obj));
		return this;
	}
	/**
	 * 添加
	 * @param strs 单个或多个字符串
	 */
	public Array push(String... strs){
		StringBuffer sb = new StringBuffer();
		if(strs != null){
			for(String str : strs){
				sb.append(str);
			}
		}
		array.add(sb.toString());
		return this;
	}
	/**
	 * 添加
	 * @param objs 单个或者多个对象
	 */
	public Array push(Object... objs){
		StringBuffer sb = new StringBuffer();
		if(objs != null){
			for(Object obj : objs){
				sb.append(instanceOf(obj));
			}
		}
		array.add(sb.toString());
		return this;
	}
	/**
	 * 返回字符串
	 * @param separator	自定义分隔符
	 * @return
	 */
	public String join(String separator){
		this.separator = separator;
		return join();
	}
	/**
	 * 清空
	 */
	public void clear(){
		array.clear();
	}
	/**
	 * 返回字符串
	 * @return 返回字符串，默认
	 */
	public String join(){
		StringBuffer sb = new StringBuffer();
		for(String str : array){
			sb.append(this.separator);
			sb.append(str);
		}
		/* 设置截取索引 */
		int index = 0;
		if(this.separator != null && this.separator.length() > 0)index = this.separator.length();
		return sb.toString().length()>0?sb.toString().substring(index):"";
	}
	
	
	/**
	 * 实例化比较
	 * @param obj
	 * @return 字符串或者空
	 */
	public String instanceOf(Object obj){
		if(obj != null){
			if(obj instanceof String){
				return obj.toString();
			}else if(obj instanceof Long){
				return obj.toString();
			}else if(obj instanceof Integer){
				return obj.toString();
			}else if(obj instanceof Float){
				return obj.toString();
			}else if(obj instanceof Double){
				return obj.toString();
			}else if(obj instanceof Boolean){
				return obj.toString();
			}else if(obj instanceof Date){
				/* 若为日期类型就默认转换 */
				return format((Date) obj);
			}else{
				System.out.println("Array Log : \""+obj.getClass().getName() +"\" not supported.");
				return null;
			}
		}else return null;
	}
	/**
	 * 
	 * @param date
	 * @return
	 */
	private String format(Date date){
		if(this.datePattern != null){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(this.datePattern);
			return simpleDateFormat.format(date);
		}
		return date.toString();
	}
	/**
	 * 转换为字符串
	 * @param array
	 * @return 字符串
	 */
	public static String toString(Object[] array){
		if(array!=null && array.length>0){
			StringBuffer sb = new StringBuffer();
			for(Object a:array){
				sb.append(",").append(a.toString());
			}
			return sb.substring(1).toString();
		}
		return null;
	}
	/**
	 * 
	 * @param array
	 * @return
	 */
	public static String toString(List<Object> array){
		if(array!=null && array.size()>0){
			StringBuffer sb = new StringBuffer();
			for(Object a:array){
				sb.append(",").append(a.toString());
			}
			return sb.substring(1).toString();
		}
		return null;
	}
	/**
	 * 获得array长度
	 * @return
	 */
	public int getLength(){
		return this.array.size();
	}
}
