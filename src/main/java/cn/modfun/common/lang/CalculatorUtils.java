package cn.modfun.common.lang;

import java.math.BigDecimal;

public class CalculatorUtils {

	/**
	 * 除法
	 * @param num1
	 * @param num2
	 * @param keep
	 * @return
	 */
	public static BigDecimal divide(Object num1,Object num2,int keep){
		BigDecimal a = new BigDecimal(num1.toString()),b = new BigDecimal(num2.toString());
		return a.divide(b, keep, BigDecimal.ROUND_DOWN);
	}
	
	public static BigDecimal chuyi(Object num1,Object num2,int keep){
		return divide(num1, num2, keep);
	}
	
	public static BigDecimal multiply(Object num1,Object num2,int keep){
		BigDecimal a = new BigDecimal(num1.toString()),b = new BigDecimal(num2.toString());
		return a.multiply(b).setScale(keep, BigDecimal.ROUND_DOWN);
	}
	
	public static BigDecimal cheng(Object num1,Object num2,int keep){
		return multiply(num1, num2, keep);
	}
	
	
	public static BigDecimal subtract(Object num1,Object num2,int keep){
		BigDecimal a = new BigDecimal(num1.toString()),b = new BigDecimal(num2.toString());
		return a.subtract(b).setScale(keep, BigDecimal.ROUND_DOWN);
	}
	
	public static BigDecimal jian(Object num1,Object num2,int keep){
		return subtract(num1,num2,keep);
	}
	
	public static BigDecimal add(Object num1,Object num2,int keep){
		BigDecimal a = new BigDecimal(num1.toString()),b = new BigDecimal(num2.toString());
		return a.add(b).setScale(keep, BigDecimal.ROUND_DOWN);
	}
}
