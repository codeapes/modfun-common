package cn.modfun.common.lang;


import java.util.HashMap;
import java.util.Map;

/**
 * map对象操作工具类
 * @author LinCH
 *
 */
public class MapUtils extends org.apache.commons.collections.MapUtils {

	@SuppressWarnings("unchecked")
	public static Object getObject(Map<String,Object> map,String... key){
		if(key!=null){
			if(key.length > 1){
				Map<String,Object> temp = map;
				for(int i=0;i<key.length-1;i++){
					if(temp.get(key[i])!=null && temp.get(key[i]) instanceof Map){
						temp = ((Map<String, Object>) temp.get(key[i]));
					}else{
						return null;
					}
				}
				return temp.get(key[key.length-1]);
			}else{
				return map.get(key[0]);
			}
		}
		return null;
	}
	/**
	 * 获得字符串对象
	 * @param map
	 * @param key
	 * @return
	 */
	public static String getStringByArray(Map<String,Object> map,String... key){
		return getString(map,key);
	}
	
	/**
	 * 获得字符串对象
	 * @param map
	 * @param key
	 * @return
	 */
	public static String getString(Map<String,Object> map,String[] key){
		Object object = getObject(map,key);
		if(object != null){
			return object.toString();
		}
		return null;
	}
	/**
	 * 获得字符串对象
	 * @param map
	 * @param defaultValue	默认值
	 * @param key
	 * @return
	 */
	public static String getString(Map<String,Object> map,String[] key,String defaultValue){
		String value = getString(map,key);
		return value == null ? defaultValue : value;
	}
	
	
	/**
	 * 获得整形
	 * @param map
	 * @param key
	 * @return
	 */
	public static Integer getIntegerByArray(Map<String,Object> map,String... key){
		return getInteger(map,key);
	}
	
	/**
	 * 获得整形
	 * @param map
	 * @param key
	 * @return
	 */
	public static Integer getInteger(Map<String,Object> map,String[] key){
		Object object = getObject(map,key);
		if(object != null){
			return Integer.parseInt(object.toString());
		}
		return null;
	}
	/**
	 * 获得整形
	 * @param map
	 * @param defaultValue	默认值
	 * @param key
	 * @return
	 */
	public static Integer getInteger(Map<String,Object> map,String[] key,Integer defaultValue){
		Integer value = getInteger(map, key);
		return value == null ? defaultValue : value;
	}
	
	/**
	 * 获得整形
	 * @param map
	 * @param key
	 * @return
	 */
	public static int getIntValueByArray(Map<String,Object> map,String... key){
		return getIntValue(map,key);
	}
	/**
	 * 获得整形
	 * @param map
	 * @param key
	 * @return
	 */
	public static int getIntValue(Map<String,Object> map,String[] key){
		Object object = getObject(map,key);
		if(object != null){
			return Integer.parseInt(object.toString());
		}
		return 0;
	}
	/**
	 * 获得整形
	 * @param map
	 * @param defaultValue	默认值
	 * @param key
	 * @return
	 */
	public static int getIntValue(Map<String,Object> map,String[] key,int defaultValue){
		Object object = getObject(map,key);
		if(object != null){
			return Integer.parseInt(object.toString());
		}
		return defaultValue;
	}
	
	/**
	 * 快速创建map对象
	 * @param keyValue
	 * @return
	 */
	public static Map<String,Object> create(Object[][] keyValue){
		Map<String,Object> map = new HashMap<String,Object>();
		if(keyValue!= null && keyValue.length >0){
			for(Object[] row : keyValue){
				if(row!=null && row.length == 2 && !StringUtils.isBlank(row[0])){
					map.put(row[0].toString(), row[1]);
				}
			}
		}
		return map;
	}
	
	/**
	 * 获得map对象
	 * @param map
	 * @param key
	 * @return
	 */
	public static Map<String,Object> getMapByArray(Map<String,Object> map,String... key){
		return getMap(map,key);
	}
	
	/**
	 * 获得map对象
	 * @param map
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getMap(Map<String,Object> map,String[] key){
		if(key!=null){
			if(key.length > 1){
				Map<String,Object> temp = map;
				for(int i=0;i<key.length-1;i++){
					if(temp.get(key[i])!=null && temp.get(key[i]) instanceof Map){
						temp = MapUtils.getMap(temp, key[i]);
					}else{
						return null;
					}
				}
				return MapUtils.getMap(temp, key[key.length-1]);
			}else{
				return MapUtils.getMap(map, key[0]);
			}
		}
		return null;
	}
	
//	public static void main(String[] args) {
//		String json = "{a:{b:{c:{d:{e:'deep'}}}},obj:{a:1,b:{c:{d:'asdf'}}}}";
//		Map<String,Object> map = new Gson().fromJson(json, Map.class);
//		System.out.println(MapUtils.getObject(map, "obj.a.b.c.d"));
//	}
}
