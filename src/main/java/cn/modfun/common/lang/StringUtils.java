package cn.modfun.common.lang;


public class StringUtils extends org.apache.commons.lang3.StringUtils {

	/**
	 * 获得字符串长度（一个汉字算两个字节）
	 * @param s
	 * @return
	 */
	public static int getLength(String s) {
		s = s.replaceAll("[^\\x00-\\xff]", "**");
		int length = s.length();
		return length;
	}
	
	/**
	 * 判断对象是否不为空，包括空格
	 * @param o
	 * @return
	 */
	public static boolean isBlank(Object o){
		return !isNotBlank(o);
	}
	
	/**
	 * 判断对象是否不为空，包括空格
	 * @param o
	 * @return
	 */
	public static boolean isNotBlank(Object o){
		if(o == null){
			return false;
		}else{
			return isNotBlank(o.toString());
		}
	}
	/** 
     * Java中1个char类型的变量可存储任意编码的1个字符，如1个ASC码和或1个中文字符， 
     * 例如：含有3个ASC和含有3个汉字字符的字符串长度是一样的： "1ac".length()==3;  "你好a".length()=3; 
     * 但上述两个字符串所占的byte是不一样的，前者是3，后者是5（1个汉字2byte）。
　       * 请编写函数:
　       *     public static String leftStr(String source, int maxByteLen) 
     * 从source中取最大maxByteLen个byte的子串。 
     * 当最后一个byte恰好为一个汉字的前半个字节时，舍弃此byte。例如： 
     *     String str="我LRW爱JAVA"; 
     *     leftStr(str,1,-1)==""; 
     *     leftStr(str,2,-1)=="我"; 
     *     leftStr(str,4,-1)=="我LR"; 
     *     leftStr(str,11,-1)=="我LRW"; 
     * 当最后一个byte恰好为一个汉字的前半个字节时，补全汉字（多取一个字节）。例如： 
     *     String str="我LRW爱JAVA"; 
     *     leftStr(str,1,1)=="我"; 
     *     leftStr(str,2,1)=="我"; 
     *     leftStr(str,4,1)=="我LR"; 
     *     leftStr(str,11,1)=="我LRW爱"; 
     * 
     *  @param  source 原始字符串 
     *  @param  maxByteLen 截取的字节数 
     *  @param  flag 表示处理汉字的方式。1表示遇到半个汉字时补全，-1表示遇到半个汉字时舍弃 
     *  @return 截取后的字符串 
     */ 
	public static String subString(String str,int maxByteLength,int flag){
		if ( str ==  null  || maxByteLength <=  0 ){ 
            return  "" ; 
        } 
        byte []  bStr = str.getBytes () ; 
        if ( maxByteLength >= bStr.length ) return  str; 
        String cStr =  new  String ( bStr, maxByteLength -  1 ,  2 ) ; 
        if ( cStr.length ()  ==  1  && str.contains ( cStr )){ 
        	maxByteLength += flag; 
        } 
        return new  String ( bStr,  0 , maxByteLength ) ; 
	}
	/**
	 * 截取字符串
	 * @param str	字符串
	 * @param maxByteLength	最大字符串长度（一个汉字算两个字节）
	 * @param completeChineseCharacters	是否保留完整的汉字（若遇到截取到半个汉字的情况下）
	 * @return
	 */
	public static String subString(String str,int maxByteLength,boolean completeChineseCharacters){
		return subString(str,maxByteLength,completeChineseCharacters?1:-1);
	}
	
	/* 去左空格 */
	public static String leftTrim(String str) {
		if (str == null || str.equals("")) {
			return str;
		} else {
			return str.replaceAll("^[　 ]+", "");
		}
	}

	/* 去右空格 */
	public static String rightTrim(String str) {
		if (str == null || str.equals("")) {
			return str;
		} else {
			return str.replaceAll("[　 ]+$", "");
		}
	}
	
	public static String getMarkString(String content, int begin, int end,String mark) {

		if (begin >= content.length() || begin < 0) {
			return content;
		}
		if (end >= content.length() || end < 0) {
			return content;
		}
		if (begin >= end) {
			return content;
		}
		String starStr = "";
		for (int i = begin; i < end; i++) {
			starStr = starStr + mark;
		}
		return content.substring(0, begin) + starStr + content.substring(end, content.length());

	}
	/** 
     * 对字符串处理:将指定位置到指定位置的字符以星号代替 
     *  
     * @param content 
     *            传入的字符串 
     * @param begin 
     *            开始位置 
     * @param end 
     *            结束位置 
     * @return 
     */ 
	public static String getMarkString(String content, int begin, int end){
		return getMarkString(content,begin,end,"*");
	}
      
    /** 
     * 对字符加星号处理：除前面几位和后面几位外，其他的字符以星号代替 
     *  
     * @param content 
     *            传入的字符串 
     * @param frontNum 
     *            保留前面字符的位数 
     * @param endNum 
     *            保留后面字符的位数 
     * @return 带星号的字符串 
     */  
  
    public static String getMarkStringByExclude(String content, int frontNum, int endNum,String mark) {  
  
        if (frontNum >= content.length() || frontNum < 0) {  
            return content;  
        }  
        if (endNum >= content.length() || endNum < 0) {  
            return content;  
        }  
        if (frontNum + endNum >= content.length()) {  
            return content;  
        }  
        String starStr = "";  
        for (int i = 0; i < (content.length() - frontNum - endNum); i++) {  
            starStr = starStr + mark;  
        }  
        return content.substring(0, frontNum) + starStr  
                + content.substring(content.length() - endNum, content.length());  
    }  
    public static String getMarkStringByExclude(String content, int frontNum, int endNum){
    	return getMarkStringByExclude(content,frontNum,endNum,"*");
    }
}
