package cn.modfun.common.file;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import cn.modfun.common.lang.Array;

/**
 * 上传抽象类
 * @author LinCH
 *
 */
@SuppressWarnings("unchecked")
public abstract class OSSUpload {

	/**
	 * 文件类型对象
	 */
	public static Map<String,String> MIME_TYPE = new HashMap<String,String>();
	
	static{
		ClassLoader classLoader=OSSUpload.class.getClassLoader();
		InputStream in=classLoader.getResourceAsStream("cn/modfun/common/file/mime-type.json");
		String str = readInputStream(in, "utf-8", "");
		MIME_TYPE = new Gson().fromJson(str, Map.class);
	}
	
	/**
	 * 读输入流
	 * @param is
	 * @param encoding
	 * @param separator
	 * @return
	 */
	public static String readInputStream(InputStream is,String encoding,String separator){
		BufferedReader br = null;
		InputStreamReader isr = null;
		Array buffer = new Array();
		try {
			isr = new InputStreamReader(is,encoding);
			br = new BufferedReader(isr);
			String line = "";
			try {
				while ((line = br.readLine()) != null) {
					buffer.push(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}finally{
			if(is!=null){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(isr!=null){
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(br!=null){
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return buffer.join(separator);
	}
	/**
	 * 上传文件
	 * @param file_name
	 * @return
	 */
	public Map<String,Object> upload(String file_name){
		return null;
	}
	/**
	 * 
	 * @param file_name
	 * @param limit_mode
	 * @param file_suffix
	 * @param limit_size
	 * @return
	 * @throws Exception 
	 */
	public Map<String,Object> upload(String file_name,String limit_mode,String[] file_suffix,Long limit_size) throws Exception{
		return null;
	}
	
	public Map<String,Object> upload(File file){
		return null;
	}
	
	public Map<String,Object> upload(File file,String limit_mode,String[] file_suffix,Long limit_size) throws Exception{
		return null;
	}
	
	/**
	 * 限制消息
	 * @author LinCH
	 *
	 */
	public static class LimitMessage {
		/**
		 * 成功状态
		 */
		private boolean success;
		/**
		 * 消息提示
		 */
		private String message;

		public boolean getSuccess() {
			return success;
		}

		public boolean isSuccess() {
			return success;
		}

		public LimitMessage setSuccess(boolean success) {
			this.success = success;
			return this;
		}

		public String getMessage() {
			return message;
		}

		public LimitMessage setMessage(String message) {
			this.message = message;
			return this;
		}

	}
	
	/**
	 * 获得文件扩展名
	 * @param file_name
	 * @return
	 */
	public static String getFileExtensionName(String file_name){
		int extension_index = file_name.lastIndexOf(".");
		extension_index = extension_index == -1?file_name.length():extension_index;
		String file_extension = file_name.substring(extension_index);
		return file_extension.toLowerCase();
	}
	
	
	/**
	 * 限制文件操作
	 * @param file_name
	 * @param file_size
	 * @param limit_mode
	 * @param file_suffix
	 * @param limit_size
	 * @return
	 */
	public static LimitMessage limit(String file_name,long file_size,String limit_mode,String[] file_suffix,Long limit_size){
		/* 设置默认值 */
		limit_mode = limit_mode == null ? "include" : limit_mode;
		limit_size = limit_size == null ? 2 * 1024 *1024 : limit_size;
		/* 获得文件扩展名 */
		String file_extension = getFileExtensionName(file_name);
		/* 判断扩展名是否有效 */
		boolean exist = false;
		List<String> asList = Arrays.asList(file_suffix);
		exist = asList.contains(file_extension);
		if("exclude".equals(limit_mode)){
			exist = !exist;
		}
		if(exist){
			/* 判断文件大小 */
			if(file_size > limit_size){
				return new LimitMessage().setSuccess(false).setMessage("文件大小超出限制");
			}
		}else{
			return new LimitMessage().setSuccess(false).setMessage("该类型文件不允许上传");
		}
		return new LimitMessage().setSuccess(true);
	}
	
	public static void closeFileChannel(FileChannel fc){
		if(fc!=null){
			try {
				fc.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void closeFileInputStream(FileInputStream fis){
		if(fis!=null){
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void closeInputStream(InputStream is){
		if(is!=null){
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static byte[] toByteArray(InputStream input) throws IOException {
	    ByteArrayOutputStream output = new ByteArrayOutputStream();
	    byte[] buffer = new byte[4096];
	    int n = 0;
	    while (-1 != (n = input.read(buffer))) {
	        output.write(buffer, 0, n);
	    }
	    return output.toByteArray();
	}
	
}
